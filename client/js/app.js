/**
 * Client side code.
 */
(function() {
    var app = angular.module("RegApp", []);

    app.controller("RegistrationCtrl", ["$http", RegistrationCtrl]);

    function RegistrationCtrl($http) {
        var self = this; // vm

        self.user = {
            email: "",
            password: "",
            fullname: "",
            gender: "",
            dateOfBirth: ""
        };

        self.displayUser = {
            email: "",
            password: "",
            fullname: "",
            gender: "",
            dateOfBirth: ""
        };

        self.registerUser = function() {
            console.log(self.user.email);
            console.log(self.user.password);
            console.log(self.user.fullname);
            console.log(self.user.gender);
            console.log(self.user.dateOfBirth);
            $http.post("/users", self.user)
                .then(function(result) {
                    console.log(result);
                    self.displayUser.email = result.data.email;
                    self.displayUser.password = result.data.password;
                    self.displayUser.fullname = result.data.fullname;
                    self.displayUser.gender = result.data.gender;
                    self.displayUser.gender = result.data.dateOfBirth;
                }).catch(function(e) {
                    console.log(e);
                });
        };
    }

})();